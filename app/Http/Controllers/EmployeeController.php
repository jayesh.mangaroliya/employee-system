<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Validator;


class EmployeeController extends Controller {

	// set index page view
	public function index() {
		return view('index');
	}

	// handle fetch all eamployees ajax request
	public function fetchAll() {
		$emps = Employee::all();
		$output = '';
		if ($emps->count() > 0) {
			$output .= '<table class="table table-striped table-sm text-center align-middle">
            <thead>
              <tr>
                <th>ID</th>
                <th>Photo</th>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Salary</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>';
			foreach ($emps as $emp) {
				$output .= '<tr>
                <td>' . $emp->e_id . '</td>
                <td><img src="storage/images/' . $emp->e_photo . '" width="50" class="img-thumbnail rounded-circle"></td>
                <td>' . $emp->e_name . '</td>
                <td>' . $emp->e_username . '</td>
                <td>' . $emp->e_email . '</td>
                <td>' . $emp->e_salary . '</td>
                <td>
                  <a href="#" id="' . $emp->e_id . '" class="text-success mx-1 editIcon" data-bs-toggle="modal" data-bs-target="#editEmployeeModal"><i class="bi-pencil-square h4"></i></a>

                  <a href="#" id="' . $emp->e_id . '" class="text-danger mx-1 deleteIcon"><i class="bi-trash h4"></i></a>
                </td>
              </tr>';
			}
			$output .= '</tbody></table>';
			echo $output;
		} else {
			echo '<h1 class="text-center text-secondary my-5">No record present in the database!</h1>';
		}
	}

	// handle insert a new employee ajax request
	public function store(Request $request) {

        // $validator = Validator::make($request->all(), [
        //     'e_name' => 'required',
        //     'e_username' => 'required|unique:users',
        //     'e_email' => 'required|unique:users',
        //     'e_password' => 'required|min:10',
        //     'e_salary' => 'required|numeric',
        //     'e_photo' => 'required',

        // ],
        // [
        //     'e_name.required'=> 'Name is Required', // custom message
        //     'e_username.required'=> 'Username is Required', // custom message
        //     'e_email.required'=> 'Email is Required',
        //     'e_password.required'=> 'Password is Required',
        //     'e_salary.required'=> 'Salary is Required',
        //     'e_photo.required'=> 'Photo is Required'
        // ]
        // );

        // if ($validator->fails()) {
        //     return response()->json(['errors' => $validator->errors()->all()]);
        // }
        // else
        // {
            $file = $request->file('e_photo');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/images', $fileName);

            $empData = ['e_name' => $request->e_name, 'e_username' => $request->e_username, 'e_email' => $request->e_email, 'e_password' => Hash::make($request->e_password), 'e_salary' => $request->e_salary, 'e_photo' => $fileName];
            Employee::create($empData);
            return response()->json([
                'status' => 200,
            ]);
        // }

	}

	// handle edit an employee ajax request
	public function edit(Request $request) {
		$id = $request->id;
		$emp = Employee::where('e_id','=',$id)->first();
		return response()->json($emp);
	}

	// handle update an employee ajax request
	public function update(Request $request) {
		$fileName = '';
		$emp = Employee::where('e_id','=',$request->emp_id)->first();
		if ($request->hasFile('e_photo')) {
			$file = $request->file('e_photo');
			$fileName = time() . '.' . $file->getClientOriginalExtension();
			$file->storeAs('public/images', $fileName);
			if ($emp->e_photo) {
				Storage::delete('public/images/' . $emp->e_photo);
			}
		} else {
			$fileName = $request->emp_avatar;
		}

		$empData = ['e_name' => $request->e_name, 'e_username' => $request->e_username, 'e_email' => $request->e_email, 'e_salary' => $request->e_salary, 'e_photo' => $fileName];
		Employee::where('e_id','=',$request->emp_id)->update($empData);
		return response()->json([
			'status' => 200,
		]);
	}

	// handle delete an employee ajax request
	public function delete(Request $request) {
		$id = $request->id;
		$emp = Employee::where('e_id','=',$id)->first();
		if (Storage::delete('public/images/' . $emp->e_photo)) {
			Employee::where('e_id','=',$id)->delete();
		}
	}
}
